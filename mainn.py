from Account import *
from GeneralReactor import *
from DoomsdayClock import *
from multiprocessing import Process

if __name__ == "__main__":
    # Now we can run a few servers 
    # Process(target=server_push, args=(server_push_port,)).start()
    Process(target=Account, args=("5556", [10001, 10002])).start()
    # Process(target=Account, args=(5558,)).start()
    # Process(target=DoomsdayClock, args=(5557,)).start()
    Process(target=GeneralReactor, args=([5556,5557],[10001,10002])).start()
    # Process(target=GeneralReactor, args=(5558,5557)).start()
