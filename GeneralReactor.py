import zmq
import sys
import time

class GeneralReactor:
    def __init__(self, ports, topics):
        self.__ports = ports
        self.__topics = topics
        print topics
        self.__sockets = []

        context = zmq.Context()

        # subscribing to publisher with zmq
        self.__sockets.append( context.socket(zmq.SUB) )
        self.__sockets[0].setsockopt(zmq.SUBSCRIBE, str(self.__topics[0]))
        self.__sockets[0].connect ("tcp://127.0.0.1:%s" 
            % self.__ports[0])

        # subscribing to another publisher
        self.__sockets.append( context.socket(zmq.SUB) )
        self.__sockets[1].setsockopt(zmq.SUBSCRIBE, str(self.__topics[1]))
        self.__sockets[1].connect ("tcp://127.0.0.1:%s" 
            % self.__ports[0])

        # polling mechanism
        self.__poller = zmq.Poller()
        self.__poller.register(self.__sockets[0], zmq.POLLIN)
        self.__poller.register(self.__sockets[1], zmq.POLLIN)

        self.start()

    def __onZeroBalance(self):
         print 'there is ZERO balance CHANGE for port', self.__ports[1]

    def __onAccountChange(self):
        print 'there is an accont CHANGE for port', self.__ports[0]

    def start(self):
        while True:

            # do something
            print 'Doing something'

            # polling with number indicting the wait in ms
            socks = dict(self.__poller.poll(500))

            if self.__sockets[0] in socks \
            and socks[ self.__sockets[0] ] == zmq.POLLIN:
                string = self.__sockets[0].recv()
                self.__onAccountChange()

            if self.__sockets[1] in socks \
            and socks[ self.__sockets[1] ] == zmq.POLLIN:
                string = self.__sockets[1].recv()
                self.__onZeroBalance()

            # time.sleep(0.5)

if __name__ == "__main__":
  GeneralReactor([5556,5557],[10001,10002])
