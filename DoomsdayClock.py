import zmq
import random
import time
import sys

class DoomsdayClock:
    def __init__(self, port):

        # zmq communications
        context = zmq.Context()
        socket = context.socket(zmq.PUB)
        socket.bind("tcp://*:{0}".format(port))
        while True:
            topic = random.randrange(9999,10005)
            messagedata = random.randrange(1,215) - 80
            print "doomsday clock sending: %d %d" % (topic, messagedata)
            socket.send("%d %d" % (topic, messagedata))
            time.sleep(1)

    def setBalance(self, value):
        self.__balance = value

if __name__ == "__main__":
  DoomsdayClock(5557)