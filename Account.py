import zmq
import random
import time

class Account:
    def __init__(self, port, topics):

        self.__topics = topics

        # socket binding using zmq
        context = zmq.Context()
        self.__socket = context.socket(zmq.PUB)
        self.__socket.bind("tcp://*:%s" % port)

        self.start()

    def start(self):

        while True:

            self.__balance = random.randrange(0,3)

            # publishing balance notifications
            print('Sending balance notifications')
            self.__socket.send( '%d %d' 
                % (self.__topics[0], self.__balance) )

            # publishing reaching zero balance
            if self.__balance == 0:
                print('Sending zero balance notification')
                self.__socket.send('%d' % self.__topics[1])

            time.sleep(0.5)

if __name__ == "__main__":
    Account(5556, [10001, 10002])